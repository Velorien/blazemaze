﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazeMaze
{
    public class MazeGenerator
    {
        public const int N = 1;
        public const int E = 2;
        public const int S = 4;
        public const int W = 8;
        private readonly Random random = new Random();
        private readonly int[] Directions = new[] { N, E, S, W };

        private readonly Dictionary<int, int> dy = new Dictionary<int, int>
            { { N, -1 }, { E, 0 }, { W, 0 }, { S, 1 } };

        private readonly Dictionary<int, int> dx = new Dictionary<int, int>
            { { N, 0 }, { E, 1 }, { W, -1 }, { S, 0 } };

        private readonly Dictionary<int, int> opposite = new Dictionary<int, int>
            { {N, S}, {S, N}, {W, E}, {E, W} };

        public int[,] Generate(int width, int height)
        {
            var maze = new int[width, height];
            void CarveFrom(int x, int y)
            {
                var dirs = Directions.OrderBy(d => random.Next());
                foreach (int d in dirs)
                {
                    int nx = x + dx[d];
                    int ny = y + dy[d];
                    if (ny >= 0 && ny < height && nx >= 0 && nx < width && maze[nx, ny] == 0)
                    {
                        maze[nx, ny] += opposite[d];
                        maze[x, y] += d;
                        CarveFrom(nx, ny);
                    }
                }
            }

            CarveFrom(0, 0);
            return maze;
        }

        private string GetClass(int walls) => string.Join(string.Empty, new string[] {
            (walls & N) == 0 ? string.Empty : " n",
            (walls & E) == 0 ? string.Empty : " e",
            (walls & S) == 0 ? string.Empty : " s",
            (walls & W) == 0 ? string.Empty : " w",
        });
    }
}
